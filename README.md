Uruchomienie projektu

1. Aby uruchmić projekt, należy mieć zainstalowanego Meteora oraz NodeJS.
    https://www.meteor.com/
    https://nodejs.org/en/
    
2. w katalogu projektu uruchomic: meteor npm install
3. w katalogu projektu uruchomic: meteor (spowoduje uruchomienie serwera i klienta web)
* aby uruchomic aplikację na Android: meteor run android-device
