export const DataFormat = {
    shortDate: 'DD.MM HH:mm:ss',
    fullDate: 'dddd, DD-MM-YYYY HH:mm:ss.SSS'
};
