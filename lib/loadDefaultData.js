import Data from "/imports/api/collections/data";
import moment from "moment";

export const loadDefaultData = () => {
    let count = 0;
    const getRandomArbitrary = (min, max) => {
        return Math.ceil(Math.random() * (max - min) + min);
    };
    for (let i = 0; i < 450; i += 1) {
        Data.insert({
            date: new Date(moment().subtract(count * 10, 'minutes')),
            temperature: getRandomArbitrary(3, 25),
            humidity: getRandomArbitrary(22, 93)
        });
        count += 1;
    }
    console.info(`Added ${count} hardcoded records added to database.`);
    return count;
};
