import '../imports/api/index';
import { loadDefaultData } from '/lib/loadDefaultData';
import Data from '/imports/api/collections/data';

Meteor.startup(() => {
    if (Data.find().count() === 0) {
        loadDefaultData();
    }
});
