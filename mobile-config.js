App.info({
    id: 'com.putpoznan.iotarduino',
    name: 'IoT Arduino',
    description: 'IoT Arduino Project',
    author: 'Jakub Stephan',
    email: 'stefik4@wp.pl'
});

App.setPreference('LoadUrlTimeoutValue', 60000);
App.setPreference('WebAppStartupTimeout', 60000);
App.setPreference('ShowUpdateScreenSpinner', true);
App.setPreference('SplashShowOnlyFirstTime', false);
App.setPreference('AndroidExtraFilesystems', 'files,files-external,documents,sdcard,cache,cache-external,assets,root');
App.setPreference('AndroidPersistentFileLocation', 'Compatibility');

App.icons({
    android_mdpi: 'public/icon.png',
    android_hdpi: 'public/icon.png',
    android_xhdpi: 'public/icon.png',
    android_xxhdpi: 'public/icon.png',
    android_xxxhdpi: 'public/icon.png'
});

App.launchScreens({
    android_mdpi_portrait: 'public/splashscreen_p.png',
    android_mdpi_landscape: 'public/splashscreen_l.png',
    android_hdpi_portrait: 'public/splashscreen_p.png',
    android_hdpi_landscape: 'public/splashscreen_l.png',
    android_xhdpi_portrait: 'public/splashscreen_p.png',
    android_xhdpi_landscape: 'public/splashscreen_l.png',
    android_xxhdpi_portrait: 'public/splashscreen_p.png',
    android_xxhdpi_landscape: 'public/splashscreen_l.png',
    android_xxxhdpi_portrait: 'public/splashscreen_p.png',
    android_xxxhdpi_landscape: 'public/splashscreen_l.png'
});
