import React, {Component} from 'react';
import { Panel, Table, Tooltip, OverlayTrigger, Button} from 'react-bootstrap';
import { withTracker } from 'meteor/react-meteor-data';
import _ from 'lodash';
import Data from '/imports/api/collections/data';
import moment from 'moment';
import { DataFormat } from '/lib/constants/dataFormat';

const returnNoData = 'No Data';

class StatisticsData extends Component {
    constructor() {
        super();
        this.showDateRange = this.showDateRange.bind(this);
    }
    renderTable() {
        const data = [
            {
                key: 'Records count',
                value: this.props.recordsCount
            },
            {
                key: 'Max temperature',
                value: this.renderElement(this.props.maxTemperature, 'temperature')
            },
            {
                key: 'Min temperature',
                value: this.renderElement(this.props.minTemperature, 'temperature')
            },
            {
                key: 'Average temperature',
                value: (!isNaN(this.props.avgTemperature) && this.props.recordsCount > 0) ? this.props.avgTemperature.toFixed(2) + '°C' : returnNoData
            },
            {
                key: 'Max humidity',
                value: this.renderElement(this.props.maxHumidity, 'humidity')
            },
            {
                key: 'Min humidity',
                value: this.renderElement(this.props.minHumidity, 'humidity')
            },
            {
                key: 'Average humidity',
                value: (!isNaN(this.props.avgHumidity) && this.props.recordsCount > 0) ? this.props.avgHumidity.toFixed(2) + '%' : returnNoData
            },
            {
                key: 'Date range',
                value: this.showDateRange()
            }
        ];
        return data.map((element) => {
            return (
                <tr key={element.key}>
                    <td>{element.key}</td>
                    <td className="center">{element.value}</td>
                </tr>
            );
        });
    }
    showDateRange() {
        if (this.props.minDate && this.props.maxDate) {
            return (
                <div>
                    From: <b>{moment(this.props.minDate.date).format(DataFormat.shortDate)}</b><br />
                    To: <b>{moment(this.props.maxDate.date).format(DataFormat.shortDate)}</b>
                </div>
            );
        }
        return returnNoData;
    }
    renderElement(data, show) {
        if (data && data.date && !isNaN(data.temperature) && !isNaN(data.humidity)) {
            const tooltip = (
                <Tooltip id="tooltip">
                    <strong>{moment(data.date).format(DataFormat.fullDate)}</strong><br />
                    Temperature: <strong>{data.temperature.toFixed(2)}°C</strong><br />
                    Humidity: <strong>{data.humidity.toFixed(2)}%</strong><br />
                </Tooltip>
            );
            return (
                <OverlayTrigger placement="top" overlay={tooltip}>
                    <span>{data[show].toFixed(2)}{show === 'humidity' ? '%' : '°C'}</span>
                </OverlayTrigger>
            );
        }
        return returnNoData;
    }

    render() {
        return (
            <Panel header="Statistics Data">
                <Table striped bordered condensed hover>
                    <thead>
                        <tr>
                            <th>Property</th>
                            <th>Value</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.renderTable()}
                    </tbody>
                </Table>
            </Panel>
        );
    }
}

export default withTracker(() => {
    const recordsCount = Data.find().count();
    let maxTemperature = 0, minTemperature = 0, avgTemperature = 0, maxHumidity = 0, minHumidity = 0, avgHumidity = 0, minDate = null, maxDate = null;
    if (recordsCount > 0) {
        maxTemperature = Data.findOne({}, {sort: {temperature: -1}});
        minTemperature = Data.findOne({}, {sort: {temperature: 1}});
        const avgTemperatureMap = Data.find().map((item) => {
            return item.temperature;
        });
        avgTemperature = _.sum(avgTemperatureMap) / avgTemperatureMap.length;
        maxHumidity = Data.findOne({}, {sort: {humidity: -1}});
        minHumidity = Data.findOne({}, {sort: {humidity: 1}});
        minDate = Data.findOne({}, {sort: {date: 1}});
        maxDate = Data.findOne({}, {sort: {date: -1}});
        const avgHumidityMap = Data.find().map((item) => {
            return item.humidity;
        });
        avgHumidity = _.sum(avgHumidityMap) / avgHumidityMap.length;
    }
    return {
        recordsCount,
        maxTemperature,
        minTemperature,
        avgTemperature,
        maxHumidity,
        minHumidity,
        avgHumidity,
        minDate,
        maxDate
    };
})(StatisticsData);
