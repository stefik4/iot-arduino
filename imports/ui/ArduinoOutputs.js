import React, { Component } from 'react';
import { Panel, Table } from 'react-bootstrap';

export default class ArduinoOutputs extends Component {
  constructor() {
    super();
    this.timer = null;
  }
  componentWillMount() {
    this.timer = Meteor.setInterval(() => {
    //  this.getData();
    }, 5000);
  }
  getData() {
    const url = 'http://api.openweathermap.org/data/2.5/weather?q=Leszno&appid=b4f4da1c6c3bc824e0531cf25ae85264';
    Meteor.call('makeRequest', url, (error, result) => {
      if (error) {
        console.log(error);
      } else {
        //Set State
      }
    });
  }
  renderTable() {
        const data = [
        ];
        return data.map((element) => {
          return (
            <tr key={element.key}><td>{element.key}</td><td>{element.value}</td></tr>
          );
        });
    }
    render() {
        return (
          <Panel header="Arduino Outputs">
              <Table striped bordered condensed hover>
                    <thead>
                    <tr>
                        <th>Key</th>
                        <th>Value</th>
                    </tr>
                    </thead>
                    <tbody>
                        {this.renderTable()}
                    </tbody>
              </Table>
          </Panel>
        );
    }
}
