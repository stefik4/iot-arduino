import React, {Component} from 'react';
import { Panel, Button } from 'react-bootstrap';

export default class LedController extends Component {
    render() {
        return (
            <Panel header="LED Controller">
                <Button bsStyle="success" bsSize="lg" className="ledButton">LED ON</Button>
                <Button bsStyle="danger" bsSize="lg" className="ledButton">LED OFF</Button>
            </Panel>
        );
    }
}
