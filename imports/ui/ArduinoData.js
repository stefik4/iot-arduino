import React, {Component} from 'react';
import {Panel, Table} from 'react-bootstrap';
import DeviceProvider from '../libs/DeviceProvider';

export default class ArduinoData extends Component {
    renderTable() {
        // TODO: dopisać jakie uzywam moduły
        const data = [
            {
                key: 'Arduino version',
                value: 'Leonardo'
            },
            {
                key: 'SD Card module',
                value: 'MH-SD Card Module'
            },
            {
                key: 'Temperature module',
                value: 'DHT11'
            },
            {
                key: 'Mobile',
                value: DeviceProvider.getManufacturer() + ' / ' + DeviceProvider.getModel()
            },
            {
                key: 'OS',
                value: DeviceProvider.getPlatform() + ' ' + DeviceProvider.getOSVersion()
            }
        ];
        return data.map((element) => {
            return (
                <tr key={element.key}>
                    <td>{element.key}</td>
                    <td>{element.value}</td>
                </tr>
            );
        });
    }

    render() {
        return (
            <Panel header="Hardware Information">
                <Table striped bordered condensed hover>
                    <thead>
                    <tr>
                        <th>Key</th>
                        <th>Value</th>
                    </tr>
                    </thead>
                    <tbody>
                    {this.renderTable()}
                    </tbody>
                </Table>
            </Panel>
        );
    }
}
