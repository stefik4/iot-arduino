import React, { Component } from 'react';
import { Grid, Row, Col } from 'react-bootstrap';

import ArduinoData from './ArduinoData';

// Components not used (was used for webserver control)
import ArduinoOutputs from './ArduinoOutputs';
import LedController from './LedController';
import Settings from './Settings';

// Components for reading data from SD Card
import DeviceProvider from './../libs/DeviceProvider';
import StatisticsData from './StatisticsData';
import FileSettings from './FileSettings';
import Charts from './Charts';
import DataManager from './DataManager';

export default class App extends Component {
    androidPermission() {
        const requiredPermissions = [
            cordova.plugins.diagnostic.runtimePermission.READ_EXTERNAL_STORAGE
        ];
        cordova.plugins.diagnostic.requestRuntimePermissions(
            (statuses) => {
                for (const permission in statuses) {
                    if (statuses[permission] !== cordova.plugins.diagnostic.permissionStatus.GRANTED) {
                        console.error(`User refused to grant permission ${permission}. Status: ${statuses[permission]}`);
                    }
                }
            },
            (error) => {
                console.error(`111 Permission request fail: ${error}`);
            },
            requiredPermissions
        );
    }
    render() {
        if (Meteor.isCordova) {
            this.androidPermission();
        }
        return (
            <Grid>
                <Row className="show-grid">
                    <Col xs={12} sm={6} md={3}><ArduinoData /></Col>
                    <Col xs={12} sm={6} md={3}><StatisticsData /></Col>
                    <Col xs={12} sm={12} md={6}><DataManager /></Col>
                </Row>
                <Row className="show-grid">
                    <Col xs={12} sm={8} md={9}><Charts /></Col>
                    <Col xs={12} sm={4} md={3}><FileSettings /></Col>
                </Row>
            </Grid>
        );
    }
}
