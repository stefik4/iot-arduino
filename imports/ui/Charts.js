import React, {Component} from 'react';
import {Panel, Form, FormControl} from 'react-bootstrap';
import {ResponsiveContainer, LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend} from 'recharts';
import {withTracker} from 'meteor/react-meteor-data';
import Data from '/imports/api/collections/data';
import moment from 'moment';
import _ from 'lodash';
import { DataFormat } from '/lib/constants/dataFormat';

import Select from 'react-select';
import 'react-select/dist/react-select.css';

class Charts extends Component {
    constructor() {
        super();
        this.state = {
            recordsLimit: 50,
            selectedOption: 0
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleChangeSelect = this.handleChangeSelect.bind(this);
    }

    handleChange(event) {
        this.setState({ recordsLimit: parseInt(event.target.value) });
    }

    handleChangeSelect(event) {
        this.setState({ selectedOption: parseInt(event.value) });
    }

    render() {
        const chartData = _.takeRight(this.props.data, this.state.recordsLimit);
        return (
            <Panel header="Charts (last data)">
                <Form inline>
                    <FormControl type="number" label="Records Limit" defaultValue={this.state.recordsLimit} onChange={this.handleChange} />
                    <Select
                      name="form-field-name"
                      value={this.state.selectedOption}
                      onChange={this.handleChangeSelect}
                      options={[
                          { value: 0, label: 'Both' },
                          { value: 1, label: 'Temperature' },
                          { value: 2, label: 'Humidity' }
                      ]}
                    />
                </Form>
                <div style={{'height': '300px', 'marginLeft': '-33px'}}>
                    <ResponsiveContainer width='100%'>
                        <LineChart data={chartData} margin={{top: 15, right: 0, left: 0, bottom: 0}}>
                            <XAxis dataKey="name"/>
                            <YAxis yAxisId="left"/>
                            <YAxis yAxisId="right" orientation="right"/>
                            <CartesianGrid strokeDasharray="3 3"/>
                            <Tooltip/>
                            <Legend/>
                            {(this.state.selectedOption == 0 || this.state.selectedOption == 1) ?
                                <Line yAxisId="left" type="monotone" dataKey="temperature" stroke="#8884d8" activeDot={{r: 8}}/>
                                :
                                null
                            }
                            {(this.state.selectedOption == 0 || this.state.selectedOption == 2) ?
                                <Line yAxisId="right" type="monotone" dataKey="humidity" stroke="#82ca9d" activeDot={{r: 8}}/>
                                :
                                null
                            }
                        </LineChart>
                    </ResponsiveContainer>
                </div>
            </Panel>
        );
    }
}


export default withTracker(() => {
    const recordsCount = Data.find().count();
    let data = [];
    if (recordsCount > 0) {
        data = Data.find({}, { sort: { date: 1 } } ).map((item) => {
            const {date, temperature, humidity} = item;
            return {
                name: moment(date).format(DataFormat.shortDate),
                temperature,
                humidity
            };
        });
        return {data};
    }
    return {
        data
    };
})(Charts);
