import React, { Component } from 'react';
import { Panel, FormGroup, ControlLabel, FormControl } from 'react-bootstrap';
import LocalStorageProvider from './../libs/LocalStorageProvider';
import { LocalStorageKeys } from '/lib/constants/localStorageKeys';

export default class FileSettings extends Component {
  constructor() {
       super();
       this.state = {
           clickCount: 0
       };
       this._localStorageProvider = new LocalStorageProvider();
       const webServerUrl = this._localStorageProvider.get(LocalStorageKeys.filePath) || '';
       if (webServerUrl.length <= 3) {
          alert('File path is not defined. Please setup it.');
       }

       this.click = this.click.bind(this);
       this.onInputChange = this.onInputChange.bind(this);
       this.getFilePath = this.getFilePath.bind(this);
   }
   shouldComponentUpdate(nextProps, nextState) {
     return this.state.clickCount !== nextState.clickCount;
   }
   click() {
     this.setState({ clickCount: this.state.clickCount + 1 });
   }
   onInputChange(event) {
     this._localStorageProvider.set(LocalStorageKeys.filePath, event.target.value);
   }
   getFilePath() {
     return this._localStorageProvider.get(LocalStorageKeys.filePath);
   }
   showSettings() {
     if (this.state.clickCount < 10) {
       return (
         <div className="settingsExecutor" onClick={this.click} />
       );
     }
     return (
       <Panel header="App Settings">
          <FormGroup controlId="formValidationSuccess1">
              <ControlLabel>Path to file with data</ControlLabel>
              <FormControl
                  type="text"
                  defaultValue={this.getFilePath()}
                  onChange={this.onInputChange}
              />
          </FormGroup>
       </Panel>
     );
   }
    render() {
        return (
          <div>
              {this.showSettings()}
          </div>
        );
    }
}
