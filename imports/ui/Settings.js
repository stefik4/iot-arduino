import React, {Component} from 'react';
import {Panel, FormGroup, ControlLabel, FormControl} from 'react-bootstrap';
import LocalStorageProvider from './../libs/LocalStorageProvider';
import {LocalStorageKeys} from '/lib/constants/localStorageKeys';

export default class Settings extends Component {
    constructor() {
        super();
        this.state = {
            clickCount: 0
        };
        this._localStorageProvider = new LocalStorageProvider();
        const webServerUrl = this._localStorageProvider.get(LocalStorageKeys.webServerUrl) || '';
        if (webServerUrl.length <= 3) {
            alert('WebServerUrl is not defined. Please setup it in AppSettings.');
        }

        this.click = this.click.bind(this);
        this.onInputChange = this.onInputChange.bind(this);
        this.getWebServerUrl = this.getWebServerUrl.bind(this);
    }

    shouldComponentUpdate(nextProps, nextState) {
        return this.state.clickCount !== nextState.clickCount;
    }

    click() {
        this.setState({clickCount: this.state.clickCount + 1});
    }

    onInputChange(event) {
        this._localStorageProvider.set(LocalStorageKeys.webServerUrl, event.target.value);
    }

    getWebServerUrl(event) {
        return this._localStorageProvider.get(LocalStorageKeys.webServerUrl);
    }

    showSettings() {
        if (this.state.clickCount < 10) {
            return (
                <div className="settingsExecutor" onClick={this.click}/>
            );
        }
        return (
            <Panel header="Settings">
                <FormGroup controlId="formValidationSuccess1">
                    <ControlLabel>Web Server URL</ControlLabel>
                    <FormControl
                        type="text"
                        defaultValue={this.getWebServerUrl()}
                        onChange={this.onInputChange}
                    />
                </FormGroup>
            </Panel>
        );
    }

    render() {
        return (
            <div>
                {this.showSettings()}
            </div>
        );
    }
}
