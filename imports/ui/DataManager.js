import React, {Component} from 'react';
import {Panel, Grid, Row, Col, Button, Alert} from 'react-bootstrap';
import {LocalStorageKeys} from '/lib/constants/localStorageKeys';

export default class DataManager extends Component {
    constructor() {
        super();
        this._LocalStorageProvider = window.LocalStorageProvider;
        this.state = {
            loadInProgress: false,
            clearInProgress: false,
            defaultLoadInProgress: false,
            error: {
                visible: false,
                content: ''
            }
        };
        this.handleError = this.handleError.bind(this);
    }

    handleError(content, errorCode) {
        this.setState({ error: { visible: true, content: content + '. Error code: ' + errorCode }, loadInProgress: false });
        Meteor.setTimeout(() => {
            this.setState({ error: { visible: false, content: '' } });
        }, 5000)
    }

    loadData() {
        const self = this;
        self.setState({loadInProgress: true});
        if (Meteor.isCordova) {
            const filePath = this._LocalStorageProvider.get(LocalStorageKeys.filePath);
            const storagePath = cordova.file.externalRootDirectory;
            window.resolveLocalFileSystemURL(storagePath, function (dir) {
                dir.getFile(filePath, {}, function (fileEntry) {
                    fileEntry.file(function (dictFile) {
                        let reader = new FileReader();
                        reader.onloadend = function (e) {
                            Meteor.call('addData', this.result.split("\n"), (error, result) => {
                                self.setState({loadInProgress: false});
                                if (error) {
                                    console.error('Error method "addData"', error);
                                } else if (!result) {
                                    console.warn('No result from method "addData".')
                                } else {
                                    console.info(`${result} records was added from database with success.`);
                                }
                            });
                        };
                        reader.readAsText(dictFile);
                    });
                }, (error) => {
                    self.handleError(`File reading error (method: getFile, storagePath: ${cordova.file.externalRootDirectory}, filePath: ${filePath}).`, error.code);
                });
            }, (error) => {
                self.handleError(`Looking for file error (method resolveLocalFileSystemURL, storagePath: ${storagePath}).`, error.code);
            });
        } else {
            self.handleError('Loading the data from SD Card is possible only on mobile devices.', 0);
        }
    }

    clearData() {
        this.setState({clearInProgress: true});
        Meteor.call('clearData', (error, result) => {
            this.setState({clearInProgress: false});
            if (error) {
                console.error('Error method "clearData"', error);
            } else if (!result) {
                console.warn('No result from method "clearData".')
            } else {
                console.info(`${result} records was removed from database with success.`);
            }
        });
    }

    loadDefaultData() {
        this.setState({defaultLoadInProgress: true});
        Meteor.call('loadDefaultData', (error, result) => {
            this.setState({defaultLoadInProgress: false});
            if (error) {
                console.error('Error method "loadDefaultData"', error);
            } else if (!result) {
                console.warn('No result from method "loadDefaultData".')
            } else {
                console.info(`${result} records loaded with success.`);
            }
        });
    }
    renderAlertError() {
        if (this.state.error.visible) {
            return (
                <Alert bsStyle="danger">
                  <h4>Wrong action!</h4>
                  <p>{this.state.error.content}</p>
                </Alert>
            );
        }
        return null;
    }
    render() {
        return (
          <div>
              {this.renderAlertError()}
              <Panel header="Data Manager">
                  <Button
                      bsSize="lg"
                      className="controlButton"
                      bsStyle="success"
                      disabled={this.state.loadInProgress}
                      onClick={!this.state.loadInProgress ? this.loadData.bind(this) : null}
                  >
                      {this.state.loadInProgress ? 'Loading' : 'Load'}
                  </Button>
                  <Button
                      bsStyle="danger"
                      bsSize="lg"
                      className="controlButton"
                      disabled={this.state.clearInProgress}
                      onClick={!this.state.clearInProgress ? this.clearData.bind(this) : null}
                  >
                      {this.state.clearInProgress ? 'Clearing' : 'Clear'}
                  </Button>
                  <Button
                      bsSize="lg"
                      className="controlButton"
                      bsStyle="warning"
                      disabled={this.state.defaultLoadInProgress}
                      onClick={!this.state.defaultLoadInProgress ? this.loadDefaultData.bind(this) : null}
                  >
                      {this.state.defaultLoadInProgress ? 'Loading' : 'Default'}
                  </Button>
                </Panel>
            </div>
        );
    }
}
