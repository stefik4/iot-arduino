export default class DeviceProvider {
    static getCordovaVersion() {
        return window.device ? window.device.cordova : 'Non-mobile';
    }
    static getManufacturer() {
        return window.device ? window.device.manufacturer : 'Non-mobile';
    }
    static getModel() {
        return window.device ? window.device.model : 'Non-mobile';
    }
    static getPlatform() {
        return window.device ? window.device.platform : 'Non-mobile';
    }
    static getSerial() {
        return window.device ? window.device.serial : 'Non-mobile';
    }
    static getUuid() {
        return window.device ? window.device.uuid : 'Non-mobile';
    }
    static getOSVersion() {
        return window.device ? window.device.version : 'Non-mobile';
    }
    static isAndroid() {
        return window && window.device && window.device.platform === 'Android';
    }
}
