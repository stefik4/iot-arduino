import { Mongo } from 'meteor/mongo';

const data = new Mongo.Collection('data');

export default data;
