import { check } from 'meteor/check';
import Data from '../collections/data';
import { loadDefaultData } from '/lib/loadDefaultData';

Meteor.methods({
    'makeRequest'(url) {
        check(url, String);
        return new Promise((resolve, reject) => {
            HTTP.call('GET', url, {}, (error, result) => {
                if (result && result.data) {
                    resolve(result.data);
                } else {
                    reject(error);
                }
            })
        });
    },
    'clearData'() {
        console.log('Data removed.');
        return Data.remove({});
    },
    'addData'(data) {
        check(data, Array);
        let count = 0;
        data.forEach((item) => {
            const data = item ? item.split('|') : [];
            let [ date, humidity, temperature ] = data;
            humidity = parseFloat(humidity);
            temperature = parseFloat(temperature);
            const isMeasurementReal = date && humidity != 0 && temperature != 0;
            if (!isMeasurementReal) {
                console.warn('Humidity and temperature are equal zero. Skipping adding data record. Object: ', data);
            } else if (date && !isNaN(humidity) && !isNaN(temperature)) {
                const prepareDate = (dateString) => {
                    const fullDateArray = dateString.split(' ');
                    const dateArray = fullDateArray[1].split('.');
                    const hourArray = fullDateArray[2].split(':');
                    return new Date(dateArray[2], (dateArray[1] - 1), dateArray[0], hourArray[0], hourArray[1], hourArray[2], 0)
                };
                Data.insert({
                    date: prepareDate(date),
                    humidity: parseInt(humidity, 10),
                    temperature: parseInt(temperature, 10)
                });
                count += 1;
            } else {
                console.warn('Object does not contain date, humidity, temperature. Object: ', data);
            }
        });
        console.log('Data added. Count:', count);
        return count;
    },
    'loadDefaultData'() {
        return loadDefaultData();
    }
});
