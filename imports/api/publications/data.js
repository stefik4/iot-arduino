import { Meteor } from 'meteor/meteor';
import Data from '../collections/data';

if (Meteor.isServer) {
    Meteor.publish('data', function publication() {
        return Data.find();
    });
}
