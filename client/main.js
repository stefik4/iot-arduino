import React from 'react';
import { Meteor } from 'meteor/meteor';
import { render } from 'react-dom';

import '../imports/startup/index';
import App from '../imports/ui/App.js';

import LocalStorageProvider from '/imports/libs/LocalStorageProvider';

Meteor.subscribe('data');

Meteor.startup(() => {
  render(<App />, document.getElementById('render-target'));
});

window.LocalStorageProvider = new LocalStorageProvider();
